CFLAGS=-ansi -pedantic -Wall -Wextra
CC?=gcc

all: test_simple test_async

test_simple: test_simple.c
	$(CC) $(CFLAGS) test_simple.c -o test_simple

test_async: test_async.c
	$(CC) $(CFLAGS) test_async.c -o test_async -lpthread

clean:
	rm test_simple test_async 2>/dev/null || true
